import os
import dash
import dash_bootstrap_components as dbc


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = dash.Dash(__name__, suppress_callback_exceptions=True, external_stylesheets=[dbc.themes.BOOTSTRAP])
server = app.server
server.secret_key = os.environ.get('secret_key', 'secret')

if __name__ == '__main__':
    app.run_server(debug=True)





