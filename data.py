# Data


import pymongo
import pandas as pd
from datetime import datetime, date, time, timedelta
import plotly.express as px



# TO Do
# add in additional condition on removing double events - user id has to be the same


path = "mongodb+srv://gjacobs:vW4ZkIuvjjiwSbWk@prod-cluster-01.mcafg.mongodb.net/"
client = pymongo.MongoClient(path)
db = client.meteor


def get_video_events(start_date, end_date, events, videos):
    collection = db["userEvents"]
    myquery = {"event": {"$in": events}, "videoId": {"$in": videos}}
    cursor = collection.find(myquery)
    data = pd.DataFrame(cursor)
    data = data[((data['timestamp'] >= start_date) & (data['timestamp'] < end_date))].reset_index(drop=True)
    return data


def remove_double_event_fires(data):
    data = data.sort_values('timestamp').reset_index(drop=True)
    data['Time'] = data['timestamp'].dt.time
    data['totalSeconds'] = [(datetime.combine(date.min, time) - datetime.min).total_seconds() for time in data['Time']]
    data['timeDiff'] = list(data['totalSeconds'].diff().abs())
    double_fire_ids = []
    for i in range(1,len(data)):
        if (data['timeDiff'][i] < 1) & (data['videoId'][i] == data['videoId'][i-1]):
            double_fire_ids.append(data['_id'][i])
    data = data[~data['_id'].isin(double_fire_ids)]
    return data


vid_list = ['VID_ALifeOnOurPlanet_trailer_3',
            'VID_WWF_Attenborough_education_film',
            'VID_GetSet_GC_promo_1',
            'VID_age_of_humans_1',
            'VID_B01_LJ01_VID05',
            'VID_Life_on_Earth_clipping_1',
            'VID_madagascar_landmarks_1',
            'VID_B01_LJ01_VID06',
            'VID_Chernobyl_classified_1',
            'VID_Attenborough_Adel_2',
            'VID_B01_LJ01_VID11',
            'VID_the_holocene_1',
            'VID_Wicked_Wednesday_1',
            'VID_indri_lemur_sound_1',
            'VID_Attenborough_94seconds_2',
            'VID_population_graph_2',
            'VID_hissing_cockroach_sound_1',
            'VID_tomato_frog_sound_1',
            'VID_red_lemur_sound_1',
            'VID_quiz_promo_Charlie_1',
            'VID_dancing_lemurs_1',
            'VID_Marine_Biologist_Career_2',
            'VID_GGC_comp_promo_1',
            'VID_biodiversity_planet_1']
            # 'VID_quiz_Friday_1',
            # 'VID_Whale_Humpback_1',
            # 'VID_chernobyl_eyewitness_1',
            # 'VID_Life_on_Earth_clipping_2',
            # 'VID_the_Anthropocene_1',
            # 'VID_chernobyl_wildlife_1',
            # 'VID_Whale_Orca_1',
            # 'VID_Attenborough_solutions_1',
            # 'VID_MaxLaManna_intro_1',
            # 'VID_climate_destruction_1',
            # 'VID_Ocean_3_Steps_Intro_1',
            # 'VID_LifeOnEarth_Africa_1',
            # 'VID_Whale_Narwhal_1',
            # 'VID_krill_scene_happy_feet_2_1',
            # 'VID_chernobyl_our_planet_1',
            # 'VID_Whale_Blue_1',
            # 'VID_Chernobyl_explained_1',
            # 'VID_top_5_smartphones_1',
            # 'VID_agent_of_change_1',
            # 'VID_Ocean_Pollution_1',
            # 'VID_Long_beaked_echidna_1',
            # 'VID_Eliminate_Waste_promo_1',
            # 'VID_Ocean_Overfishing_1',
            # 'VID_chernobyl_accident_1',
            # 'VID_gorilla_chimpanzee_1',
            # 'VID_our-planet_biodiversity_1']


event_types = ['videoPlay', 'videoPause', 'videoEnd']

video_stats_data = remove_double_event_fires(get_video_events('2020-10-31', '2020-12-04', event_types, vid_list))

