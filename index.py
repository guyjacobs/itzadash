import dash
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import dash_html_components as html
from layouts import videos_layout
import callbacks
from app import app, server



PLOTLY_LOGO = "https://images.plot.ly/logo/new-branding/plotly-logomark.png"

navbar = dbc.Navbar(
    children=
        [
            html.Img(src=PLOTLY_LOGO, height="30px"),
            dbc.NavLink("Videos", href="/videos", className='first-nav-link'),
        ]
)

app.layout = dbc.Container(
    [
        navbar,
        dcc.Location(id='url', refresh=False),
        html.Div(id='page-content')
    ], fluid=True
)

index_page = html.Div(id='index')

@app.callback(
    dash.dependencies.Output('page-content', 'children'),
    [
        dash.dependencies.Input('url', 'pathname')
    ]
)
def display_page(pathname):
    if pathname == '/videos':
        return videos_layout
    else:
        return index_page




if __name__ == '__main__':
    app.run_server(debug=True)