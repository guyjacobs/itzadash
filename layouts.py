
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from data import vid_list
from datetime import date





video_select_control = dcc.Dropdown(id='video-select', 
                                 options=[{"label": col, "value": col} for col in vid_list], 
                                 value='VID_WWF_Attenborough_education_film',
                                 multi=False,
                                 clearable = False)


date_range_control = dcc.DatePickerRange(id='date-select',
                                         min_date_allowed=date(2020, 11, 1),
                                         max_date_allowed=date(2021, 12, 5),
                                         initial_visible_month=date(2020, 11, 1),
                                         start_date=date(2020, 11, 1),
                                         end_date=date(2021, 12, 5),
                                         display_format='DD-MM-Y',)



video_left_column = html.Div(
    [
        dbc.Row(
            dbc.Col(
                html.Div(
                    [
                        dbc.Label("Select a video:", className='control-label'),
                        video_select_control
                    ]
                ), width=12
            )
        ),
        dbc.Row(
            dbc.Col(
                html.Div(
                    [
                        dbc.Label("Select a date range:", className='control-label'),
                        date_range_control
                    ]
                ), width=12, className='mt-4'
            )
        ),
        dbc.Row(
            dbc.Col(
                html.Div(
                    [
                        html.P('Plays', className = 'stat-label'),
                        html.P(id='unique-views', className='stat-box')
                    ], 
                ), width = 12, className = 'd-flex justify-content-center mt-5'
            )
        ),
        dbc.Row(
            [
                dbc.Col(
                    html.Div(
                        [
                            html.P('Conversion rate', className = 'stat-label'),
                            html.P(id='conversion-rate', className = 'stat-box')
                        ]
                    ), width=6, className='d-flex justify-content-center'
                ),
                dbc.Col(
                    html.Div(
                        [
                            html.P('Length', className = 'stat-label'),
                            html.P(id='video-length', className = 'stat-box')
                        ]
                    ), width=6, className='d-flex justify-content-center'
                )
            ], className='mt-4'
        ),
       
    ]
)


video_right_column = html.Div(dcc.Graph(id="video-playthrough", config = {'displayModeBar': False}))





videos_layout = dbc.Container(
    [
        dbc.Row(
            [
                dbc.Col(
                    [
                        video_left_column
                    ], width=5, lg=5, sm=5, className='left-column'
                ),
                dbc.Col(
                    [
                        video_right_column
                    ], width=7,lg=7, sm=7, className='right-column'
                )
            ]
        )
    ],  fluid=True, className='information-panel'
)





























