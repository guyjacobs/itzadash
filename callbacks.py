
import dash
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State
import numpy as np
import plotly.express as px
from data import *
from app import app


@app.callback(
    dash.dependencies.Output('unique-views', 'children'),
    [
        dash.dependencies.Input('video-select', 'value'),
        dash.dependencies.Input('date-select', 'start_date'),
        dash.dependencies.Input('date-select', 'end_date')
    ]
)
def get_unique_video_views(video_id, start_date_str, end_date_str):
    
    start_date = datetime.strptime(start_date_str,'%Y-%m-%d')
    end_date = datetime.strptime(end_date_str,'%Y-%m-%d')

    data = video_stats_data[((video_stats_data['timestamp'] >= start_date) & (video_stats_data['timestamp'] < end_date))].reset_index(drop=True)

    data = data[data['event'] == 'videoPlay']
    data = data.groupby(['userId', 'videoId']).count().reset_index()
    data = pd.DataFrame(data['videoId'].value_counts()).reset_index(level=0)
    data.columns = ['videoId', 'count']
    value = data[data['videoId'] == video_id]['count'].values[0]
    return f'{value}'




@app.callback(
    dash.dependencies.Output('conversion-rate', 'children'),
    [
        dash.dependencies.Input('video-select', 'value'),
        dash.dependencies.Input('date-select', 'start_date'),
        dash.dependencies.Input('date-select', 'end_date')
    ]
)
def get_conversion_rate(video_id, start_date_str, end_date_str):

    start_date = datetime.strptime(start_date_str,'%Y-%m-%d')
    end_date = datetime.strptime(end_date_str,'%Y-%m-%d')

    data = video_stats_data[((video_stats_data['timestamp'] >= start_date) & (video_stats_data['timestamp'] < end_date))].reset_index(drop=True)

    events_play = data[data['event'] == 'videoPlay']
    events_play = events_play.groupby(['userId', 'sessionId', 'videoId']).count().reset_index()
    events_play = pd.DataFrame(events_play['videoId'].value_counts()).reset_index(level=0)
    events_play.columns = ['videoId', 'count']

    events_end = data[data['event'] == 'videoEnd']
    events_end = events_end.groupby(['userId', 'sessionId', 'videoId']).count().reset_index()
    events_end = pd.DataFrame(events_end['videoId'].value_counts()).reset_index(level=0)
    events_end.columns = ['videoId', 'count']

    conversion_table = events_play.merge(events_end, on = 'videoId')
    conversion_table.columns = ['videoId', 'count_play', 'count_end']
    conversion_table['conv_rate'] = conversion_table['count_end'] / conversion_table['count_play']

    value = conversion_table[conversion_table['videoId'] == video_id]['conv_rate'].values[0]

    return f'{round(value*100)}%'



@app.callback(
    dash.dependencies.Output('video-length', 'children'),
    [
        dash.dependencies.Input('video-select', 'value')
    ]
)
def get_video_length(video_id):
    length = int(np.max(video_stats_data[video_stats_data['videoId'] == video_id]['videoTimecode'].values))
    time = str(timedelta(seconds = length)) 
    return f'{time[2:]}' 





@app.callback(
    dash.dependencies.Output('video-playthrough', 'figure'),
    [
        dash.dependencies.Input('video-select', 'value'),
        dash.dependencies.Input('date-select', 'start_date'),
        dash.dependencies.Input('date-select', 'end_date')
    ]
)
def update_videos(video_id, start_date_str, end_date_str):

    viewer_list = []
    percentage_viewed_list = []

    start_date = datetime.strptime(start_date_str,'%Y-%m-%d')
    end_date = datetime.strptime(end_date_str,'%Y-%m-%d')

    data = video_stats_data[((video_stats_data['timestamp'] >= start_date) & (video_stats_data['timestamp'] < end_date))].reset_index(drop=True)

    events_vid = data[data['videoId'] == video_id]

    video_length = max(events_vid['videoTimecode']) # get proper video length
    viewers = list(set(events_vid['userId']))


    for viewer in viewers:
        
        video_viewed = max(events_vid[events_vid['userId'] == viewer]['videoTimecode'].values)
        percentage_viewed = (video_viewed/video_length) * 100

        viewer_list.append(viewer)
        percentage_viewed_list.append(percentage_viewed)

    table = pd.DataFrame({'userId': viewer_list, 'percWatched': percentage_viewed_list})
    
    for i in range(len(table)):
        if table['percWatched'][i] < 0:
            table['percWatched'][i] = 0

    fig = px.histogram(table, x="percWatched", nbins = 6, histnorm='percent')

    fig.update_xaxes(title_text='Percentage of video watched', showgrid=False, color = 'white')
    fig.update_yaxes(title_text='Percentage of total viewers', showgrid=False, color = 'white')


    fig.update_layout(
        height = 680,
        width = 700,
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)',
        xaxis=dict(ticksuffix="%"),
        yaxis=dict(ticksuffix="%  "),
        font=dict(color="white"),
        title={
        'text': "Video play-through",
        'y':0.9,
        'x':0.5,
        'xanchor': 'center',
        'yanchor': 'top'}
        
    )

    fig.update_traces(
        xbins=dict( # bins used for histogram
            start=0.0,
            end=100,
            size=10
        )
    )
    return fig



